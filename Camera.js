import * as React from 'react';
import { Text, View, StyleSheet, Button } from 'react-native';
import Constants from 'expo-constants';
import * as Permissions from 'expo-permissions';
import Spinner from 'react-native-loading-spinner-overlay'
import { BarCodeScanner } from 'expo-barcode-scanner';

export default class BarcodeScannerExample extends React.Component {
  state = {
    hasCameraPermission: null,
    scanned: false,
  };

  async componentDidMount() {
      const {status} = await Permissions.askAsync(Permissions.CAMERA)
      this.setState({hasCameraPermission: status === 'granted'})
  }

  handleBarCodeScanned = ({ type, data }) => {
    this.setState({scanned: true})
    this.props.navigation.navigate({routeName: 'Data', params: {
      id: data
    }})
  }

  render() {
    const { hasCameraPermission, scanned } = this.state;

    if (hasCameraPermission === null) {
      return <Text>Requesting for camera access</Text>;
    }
    if (hasCameraPermission === false) {
      return <Text>No access to camera</Text>;
    }
    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'column',
          alignItems: "center",
          justifyContent: 'center',
          margin: 'auto',
          height: '100%',
          display: "flex",
          width: '100%'
        }}>
        <Spinner visible={!this.state.scanned} animation="none" textContent={"Scanning..."} />
        <BarCodeScanner
          onBarCodeScanned={scanned ? undefined : (this.handleBarCodeScanned)}
          style={StyleSheet.absoluteFillObject}
        />
      </View>
    );
  }
}

{/* <Spinner visible={true} textContent={"Scanning..."} />; */}
import React from 'react'
import { View, Text, TextInput, StyleSheet, Button } from 'react-native'
import Welcome from './Welcome';
import Home from './Home';
import Nav from './Nav';

class Splash extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            loaded: false
        }
    }
    componentDidMount() {
        setTimeout(() => {
            this.setState({ loaded: true })
        }, 1000)
    }
    render() {
        const { loaded } = this.state
        if (!loaded) return <Welcome />;
        return (
            <Nav />
        );
    }

}

const styles = StyleSheet.create({})

export default Splash
import React from './node_modules/react'
import {View, Text, StyleSheet, Button} from 'react-native'
import { Table, Row } from "./node_modules/react-native-table-component";
const Data = props => {
    const id = props.navigation.getParam("id");
    const name = ['product name'] //data recieved form get request
    const description = ['Data description']
    return (
      <View style={styles.container}>
      <Text>{id}</Text>
        <Table borderStyle={{borderWidth: 2, borderColor: 'black'}}>
          <Row data={name} style={styles.head}/>
          <Row data={description} style={styles.text}/>
        </Table>
        <Button title='Purchase' onPress={() => {
          props.navigation.navigate({routeName: 'Payment'})
        }}/>
        <Button title='Pop to top' onPress={() => {
          props.navigation.popToTop()
        }} />
      </View>
    );
}

const styles = StyleSheet.create({
  container:{
    flex:1,
    padding: 16,
    paddingTop: 40,
    backgroundColor: '#ffffff'
  },
  head:{
    height: 40, backgroundColor: '#f1f8ff',
  },
  text: {
    justifyContent: "flex-start",
    height: 200,
    width: '100%'
  }
})

export default Data
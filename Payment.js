import React from 'react'
import {View, Text, StyleSheet, Button} from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'

const Payment = () => {
    return (
      <View style={styles.screen}>
        <TouchableOpacity style={styles.card}>
          <View>
            <Text>Cash On Delivery</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity style={styles.card}>
          <View>
            <Text>PayTM</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity style={styles.card}>
          <View>
            <Text>UPI</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
}

const styles = StyleSheet.create({
    screen:{
        flex: 1,
        alignItems: "center",
        justifyContent: "space-evenly"
    },
    card: {
        height: 200,
        width: 200,
        textAlign: "center",
        borderColor: 'black',
        borderWidth: 3
    }
})

export default Payment
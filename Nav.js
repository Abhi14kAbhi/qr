import React, { Component } from './node_modules/react'
import {View, Text, Button} from 'react-native'
import {createAppContainer} from 'react-navigation'
import {createStackNavigator} from 'react-navigation-stack'
import Home from './Home'
import Camera from './Camera'
import Welcome from './Welcome'
import Data from './Data'
import Payment from './Payment'
const Nav = createStackNavigator({
    
    Home:{
        screen: Home,
        navigationOptions: {
            title: 'Home    '
        }},
    Camera:{
        screen: Camera,
        navigationOptions:{
            title: 'Scan Code    '
        }
    },
    Welcome: {
        screen: Welcome
    },
    Data: {
        screen: Data,
        navigationOptions: {
            title: 'Details    '
        }
    },
    Payment: {
        screen: Payment
    }
})

export default createAppContainer(Nav);

import React from 'react';
import { View, Button, Text, StyleSheet } from 'react-native'
import {HomeStyle as styles} from './styles/HomeStyle'

const Home = props => {
  return(
    <View style={styles.screen}>
      <View>
        <Text style={styles.welcomeText}>Welcome to Adrixus</Text>
      </View>
      <Text style={{paddingTop: 300}}>
        Press the button to scan th QR code
      </Text>
      <View style = {styles.buttonViewStyle}>
        <Button
          style={styles.buttonStyle}
          title="Scan."
          onPress={() => {
            props.navigation.navigate({ routeName: "Camera" });    //change the routeName to Camera after Data Styling
          }}
        />
      </View>
    </View>
  )
}

export default Home
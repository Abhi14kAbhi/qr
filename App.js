import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Nav from './Nav'
import Splash from './Splash'

const App = () => {
  return (
    <Splash />
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default App

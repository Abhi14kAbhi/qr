import React from 'react'
import { View, Text, ImageBackground, StyleSheet, Button } from 'react-native'

const Welcome = props => {
    return (
        <View style={styles.screen}>
            <ImageBackground source={{ uri: 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/20/Spaghetti_Bolognese_mit_Parmesan_oder_Grana_Padano.jpg/800px-Spaghetti_Bolognese_mit_Parmesan_oder_Grana_Padano.jpg' }} style={styles.bg} />
        </View>
    )
}

const styles = StyleSheet.create({
    screen: {
        width: '100%'
    },
    bg: {
        width: '100%',
        maxWidth: '100%',
        height: '100%',
        justifyContent: "flex-end"
    },
})

export default Welcome
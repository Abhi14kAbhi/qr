export const HomeStyle = {
    screen: {
        flex: 1,
        alignItems: "center",
        justifyContent: "flex-start",
        borderWidth: 2,
        borderColor: 'red'
    },
    welcomeViewText: {
        width: '80%'
    },  
    welcomeText: {
        fontSize: 20,
        fontStyle: 'italic'
    },
    buttonViewStyle: {
        width: '20%',
        height: '5%',
        justifyContent: "center",
        alignItems: "center",

    },
    buttonStyle: {
        backgroundColor: 'blue',
        color: 'white'
    } 
}